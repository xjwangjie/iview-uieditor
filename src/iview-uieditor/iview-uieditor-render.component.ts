
import _ from 'lodash';
import { UEOption, UEVue, UEVueComponent, UEVueProp } from 'vue-uieditor';
import './transfer';

@UEVueComponent({})
export default class IViewUieditorRender extends UEVue {
  @UEVueProp()
  private json!: string;

  @UEVueProp()
  private tmpl!: string;

  @UEVueProp()
  private mixin!: any;

  @UEVueProp()
  private query!: any;

  @UEVueProp()
  private params!: any;

  @UEVueProp(Boolean)
  private editing!: boolean;

  @UEVueProp(Boolean)
  private preview!: boolean;

  @UEVueProp()
  private options!: UEOption;
  get optionEx() {
    const options = this.options;
    this.$props
    return options || {};
  }


}

