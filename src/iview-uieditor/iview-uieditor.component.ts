import _ from 'lodash';
import { UEOption, UETheme, UEVue, UEVueComponent, UEVueProp } from 'vue-uieditor';
import './transfer';



@UEVueComponent({

})
export default class IViewUieditor extends UEVue {

  @UEVueProp()
  private options!: UEOption;

  get optionEx() {
    const options = this.options;
    return options || {};
  }

  @UEVueProp()
  private json!: UEOption;

  @UEVueProp()
  private tmpl!: UEOption;

  @UEVueProp()
  private theme!: UETheme;
  get themeEx(): UETheme {
    return _.assign({
      about({ service }) {
        return `<a href="${process.env.VUE_APP_UE_HOMEPAGE}" target="_blank">${process.env.VUE_APP_UE_NAME.toUpperCase()} ${process.env.VUE_APP_UE_VERSION} (2021)</a><div>${process.env.VUE_APP_UE_DESC}</div>`;
      }
    } as UETheme, this.theme);
  }

}
