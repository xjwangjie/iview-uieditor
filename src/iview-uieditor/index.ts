// 导入颜色选择器组件
// import vueUieditor from "./iview-uieditor.component.vue";

import Vue from 'vue';
import VueUieditor from 'vue-uieditor';
import { IViewUEBaseComponents } from './components/base.component';
import iviewUieditorRender from './iview-uieditor-render.component.vue';
import iviewUieditor from './iview-uieditor.component.vue';
import './style.less';

Vue.use(VueUieditor);

// 存储组件列表
const components: any = {
  'iview-uieditor': iviewUieditor,
  'iview-uieditor-render': iviewUieditorRender,
  ...IViewUEBaseComponents
};
// 定义 install 方法，接收 Vue 作为参数。如果使用 use 注册插件，则所有的组件都将被注册
const install: any = function (Vue: any) {
  // 判断是否安装
  if (install.installed) return;
  Object.keys(components).forEach(function (name) {
    Vue.component(name, components[name]);
  });

};
// 判断是否是直接引入文件
if (typeof window !== "undefined" && window.Vue) {
  install(window.Vue);
}

export const IViewUieditor = {
  // 导出的对象必须具有 install，才能被 Vue.use() 方法安装
  install,
  // 以下是具体的组件列表
  components
};

export default IViewUieditor;

